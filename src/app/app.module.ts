import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { HotelComponent } from './hotel/hotel.component';
import { ListHotelComponent } from './list-hotel/list-hotel.component';
import { AddHotelComponent } from './add-hotel/add-hotel.component';
import { EditHotelComponent } from './edit-hotel/edit-hotel.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    HotelComponent,
    ListHotelComponent,
    AddHotelComponent,
    EditHotelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
