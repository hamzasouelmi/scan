export class Hotel{

    constructor(
        public idHotel :number,
        public nomHotel: string,
        public descHotel: string,
        public imageUR: string,
        public nbLitsOcc:number,
        public nbLitsMax:number,
        public isdespo:boolean,
        public nblikes:number,
        public wifi?:string,
        public parking?:boolean
    ){}
}