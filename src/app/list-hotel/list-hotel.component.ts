import { Component, OnInit } from '@angular/core';
import { Hotel } from '../model/hotel';
@Component({
  selector: 'app-list-hotel',
  templateUrl: './list-hotel.component.html',
  styleUrls: ['./list-hotel.component.css']
})
export class ListHotelComponent implements OnInit {
 /* hotel1!:Hotel;
  hotel2!:Hotel;
  hotel3!:Hotel;
  hotel4!:Hotel;
  hotel5!:Hotel;
  hotel6!:Hotel;
  hotel7!:Hotel;*/
  tabHotel!:Hotel[];

  constructor() { }

  ngOnInit(): void {

    this.tabHotel=[

      {
        idHotel:1,
        nomHotel:"Sousse palm",
        descHotel:"Hotel a Sousse",
        imageUR:"assets/images/4.jpg",
        nbLitsOcc:500,
        nbLitsMax:1000,
        isdespo:true,
        nblikes:60,
        wifi:"gratuit",
        parking:true
      },
      {
        idHotel:2,
        nomHotel:"Kairouam palm",
        descHotel:"Hotel a Kairouam",
        imageUR:"assets/images/5.jpg",
        nbLitsOcc:400,
        nbLitsMax:500,
        isdespo:true,
        nblikes:100,
        wifi:"",
        parking:true
      },
      {
        idHotel:1,
        nomHotel:"Mahdia palm",
        descHotel:"Hotel a Mahdia",
        imageUR:"assets/images/6.jpg",
        nbLitsOcc:200,
        nbLitsMax:200,
        isdespo:false,
        nblikes:25,
        wifi:"disponible",
        parking:false
      },
      {
        idHotel:1,
        nomHotel:"Seliana palm",
        descHotel:"Hotel a Seliana",
        imageUR:"assets/images/7.jpg",
        nbLitsOcc:300,
        nbLitsMax:1000,
        isdespo:true,
        nblikes:40,
        wifi:"",
        parking:false
      },
      {
        idHotel:1,
        nomHotel:"Tunis palm",
        descHotel:"Hotel a Tunis",
        imageUR:"assets/images/8.jpg",
        nbLitsOcc:500,
        nbLitsMax:1000,
        isdespo:true,
        nblikes:50,
        wifi:"gratuit",
        parking:true
      },
      {
        idHotel:1,
        nomHotel:"Tozeur palm",
        descHotel:"Hotel a Tozeur",
        imageUR:"assets/images/9.jpg",
        nbLitsOcc:500,
        nbLitsMax:1000,
        isdespo:true,
        nblikes:0,
        wifi:"disponible dans les chambres",
        parking:false
      },
      {
        idHotel:1,
        nomHotel:"Bizerte palm",
        descHotel:"Hotel a Bizerte",
        imageUR:"assets/images/10.jpg",
        nbLitsOcc:500,
        nbLitsMax:1000,
        isdespo:true,
        nblikes:0,
        wifi:"",
        parking:true
      }
    ]
    
    /*this.hotel1=new Hotel(2,"Sousse palm","Hotel a Sousse","assets/images/4.jpg",500,1000,true,"disponible");
    this.hotel2=new Hotel(3,"Kairouam palm","Hotel a Kairouam","assets/images/5.jpg",300,700,true,"gratuit",false);
    this.hotel3=new Hotel(4,"Mahdia palm","Hotel a Mahdia","assets/images/6.jpg",200,500,true,"",true);
    this.hotel4=new Hotel(5,"Seliana palm","Hotel a Seliana","assets/images/7.jpg",500,1000,true,"disponible en chambre",true);
    this.hotel5=new Hotel(6,"Tozeur palm","Hotel a Tozeur","assets/images/8.jpg",500,1000,true,"",false);
    this.hotel6=new Hotel(7,"Tunis palm","Hotel a Tunis","assets/images/9.jpg",500,1000,true);
    this.hotel7=new Hotel(8,"Bizert palm","Hotel a Bizert","assets/images/10.jpg",500,1000,true);*/
  }

}
