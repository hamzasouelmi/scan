<?php

namespace App\Http\Controllers;

use App\Models\plage ;
use Illuminate\Http\Request;
use IPTools\Range;
use IPTools\IP;
class plagecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plages = plage::all();

        return view('plages', compact('plages'));
       /* $start = IP::parse('192.168.1.1');
        $end = IP::parse('192.168.1.10');
        $range = new Range($start, $end);
        
        foreach ($range as $ip) {
            echo $ip . ' ';
        }
        $start_ip = ip2long('192.168.1.1');
        $end_ip = ip2long('192.168.1.254');

while($start_ip <= $end_ip){
  echo long2ip($start_ip).'<br>';
  $start_ip++;
}*/
//return view('plages');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('plages');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        plage::create([
            'debut'=>$request->input('debut'),
            'fin'=>$request->input('fin'),
            
        ]);
        return redirect('/plages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       /* return view('plages')
        ->with('plages',plage::where('id',$id)->first());}*/
        return view('plages', [
            'plages' => plage::find($id)
            ]);}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // return view('editplages', ['plages' => plage::find($id) ]);}
        /*return view('editplages',compact ('plages'))
    ->with('plages',plage::where('id',$id)->first());*/
// $plages = DB::select('select * from plages');
plage::findOrFail($id);
//return view('editplages',compact('plages'));
return view('editplages',['plages'=>$plages]);
}
    
      
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { $plages = plage::all();
        plage::where('id',$id)->update([
            'debut'=>$request->input('debut'),
            'fin'=>$request->input('fin'),
            
        ]);
        return redirect('/plages'.$id)
        ->with('message','تم تعديل الصفحة');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
