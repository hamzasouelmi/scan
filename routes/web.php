<?php

use App\Http\Controllers\plagecontroller;
use Illuminate\Support\Facades\Route;
use App\Models\plage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::get('plages', plageC;
//Route::post('plages', [plage::class,'store']);
Route::resource('/plages',plagecontroller::class);
Route::get('click_edit','plagecontroller@edit');