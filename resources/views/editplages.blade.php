<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plages</title>
</head>
<body>


<h1 class="row justify-content-center">Editer une plage</h1>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <form class="row g-3" action="{{ url('plages/'. $plages->id) }}" method="POST">
                @csrf 
                @method('PATCH')
                <div class="col-md-6">
                    <label for="input_number" class="form-label">
                        <h5>debut</h5>
                    </label>
                    <input type="text" class="form-control"  name="debut" id="debut" value="{{$plages->debut}}">
                </div>
                <div class="col-md-6">
                    <label for="inputnumber" class="form-label">
                        <h5>fin</h5>
                    </label>
                    <input type="text" class="form-control"  name="fin" id="fin" value="{{$plages->fin}}">
                </div>
                    <button type="submit" class="btn btn-primary">editer la plage</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>