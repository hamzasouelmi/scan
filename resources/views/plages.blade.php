


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plages</title>
</head>
<body>
@if(session()->has('message'))
<div class="alert alert-danger" role="alert">
{{session()->get('message')}}</div>
@endif


<div class="container">
  <h1 class="text-center">Gestion des plages</h1>
<form method="post" action="/plages">
  @csrf
  
  <div class="form-group">
    <label >DEBUT DES PLAGES</label>
    <input type="text" name="debut"class="form-control" id="exampleInputnum"  placeholder="Enter debut">
    
  </div>
  <div class="form-group">
  <label >FIN DES PLAGES</label>
    <input type="text" name="fin"class="form-control" id="exampleInputnum"  placeholder="Enter fin">
    
  </div>
  <button type="submit" class="btn btn-primary mt-3 text-center">AJOUTER</button>
  <button type="submit" class="btn btn-primary mt-3 text-center">SCAN</button>
  <hr>
  <table class="table table-success table-striped">
  
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">id</th>
      <th scope="col">debut</th>
      <th scope="col">fin</th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
   
  </thead>
  <tbody>
  @foreach( $plages as $pl)
    <tr>
    <th scope="col">#</th>
      <th scope="row">{{$pl->id}}</th>
      <td>{{$pl->debut}}</td>
      <td>{{$pl->fin}}</td>
     
      <td><a href="#" class="btn btn-info"> consulter</a></td>
      <td><a href="click_edit/{{$pl->id}}" class="btn btn-success"> modifier</a></td>
      <td><a href="#" class="btn btn-danger"> supprimer</a></td>
    </tr>
    @endforeach
   
  </tbody>

</table>
</form>  
</div>
</body>
</html>
<!--{{url('plages/'.'editplages')}}-->